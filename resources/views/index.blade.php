@extends('layouts.app')
@section('title', 'Tasks')
@section('content')

<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <a class="btn btn-primary mt-5 mb-2" href="{{ route('task.create') }}" role="button">Create new task</a>
        @foreach($tasks as $task)
            <tr>
                <th scope="row">{{ $loop->iteration}}</th>
                <td>{{ $task->title }}</td>
                <td>{{ $task->description }}</td>
                <td>
                    <a href="{{ route('task.show', ['id' => $task->id]) }}" class="btn btn-success"><i class="far fa-eye" style="margin-right: 5px"></i></a>
                    <a href="{{ route('task.edit', ['id' => $task->id]) }}" class="btn btn-success"><i class="far fa-edit" style="margin-right: 5px"></i></a>
                    <a href="{{ route('task.destroy', ['id' => $task->id]) }}" class="btn btn-danger" onclick="return confirm('Вы уверены, что хотите удалить Task?')"><i class="far fa-trash-alt" style="margin-right: 5px"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection