@extends('layouts.app')
@section('title', 'Tasks')
@section('content')

<div class="container">
    <h1>{{ $task->title }}</h1>
    <p>{{ $task->description }}</p>
</div>
@endsection