@extends('layouts.app')
@section('title', 'Tasks')
@section('content')

<div class="container">
<form action="{{ route('task.update', ['id' => $task->id]) }}" method="POST">
{{ csrf_field() }}
<div class="mb-3 mt-5">
  <label for="exampleFormControlInput1" class="form-label">Title</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" value="{{ $task->title }}" placeholder="title">
</div>
<div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Description</label>
  <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="description">{{ $task->description }}</textarea>
</div>
<input type="hidden" name="user_id" value="{{ Auth::user()->id }}" class="form-control" id="exampleFormControlInput1" placeholder="title">
<button type="submit" class="btn btn-success">Create</button>
</form>
</div>
@endsection