<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = auth()->user()->tasks()->get()->sortByDesc('id');
        return view('index', [
            'tasks' => $tasks,
        ]);
    }

    public function show(Request $request, $id)
    {
        $task = Task::find($id);
        return view('show', [
            'task' => $task
        ]);
    }
    
    
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        Task::create($request->all());

        return redirect()->route('task.index');
    }

    public function edit($id)
    {
        return view('edit', [
            'task' => Task::find($id)
        ]);
    }


    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->update($request->all());

        return redirect()->route('task.index');
    }

    public function destroy($id)
    {
        Task::destroy($id);

        return redirect()->route('task.index');
    }
}
