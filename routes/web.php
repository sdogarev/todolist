<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('task', '\App\Http\Controllers\TaskController@index')->name('task.index');
Route::get('task/create', '\App\Http\Controllers\TaskController@create')->name('task.create');
Route::post('task/store', '\App\Http\Controllers\TaskController@store')->name('task.store');
Route::get('task/{id}/show', '\App\Http\Controllers\TaskController@show')->name('task.show');
Route::get('task/{id}/edit', '\App\Http\Controllers\TaskController@edit')->name('task.edit');
Route::post('task/{id}/update', '\App\Http\Controllers\TaskController@update')->name('task.update');
Route::get('task/{id}/destroy', '\App\Http\Controllers\TaskController@destroy')->name('task.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
